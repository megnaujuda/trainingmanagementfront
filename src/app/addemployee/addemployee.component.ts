import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgserviceService} from '../ngservice.service';
import {Employee} from 'src/app/employee/Employee';
import { HttpClient } from '@angular/common/http';
import {Factory} from 'src/app/factory/Factory';
import {TmgCommunicationService} from 'src/app/communication/tmg-communication.emitter'


@Component({
  selector: 'app-addemployee',
  templateUrl: './addemployee.component.html',
  styleUrls: ['./addemployee.component.css']
})
export class AddemployeeComponent implements OnInit {
  
  employee : Employee = new Employee();
  factory:Factory;
  submitted = false;
  constructor(
    private http:HttpClient,
    private _route:Router,
    private _service: NgserviceService,
    private  tmgCommunicationService: TmgCommunicationService
  ) { }

  ngOnInit(): void {

      this._service.getFactory().subscribe(
        data=>{
          this.factory=data as Factory;
        }
      );
  }

  newEmployee(): void {
    this.submitted = false;
    this.employee = new Employee();
  }
  
  save() {
    console.log(this.employee)
    this._service.addEmployeeToRemote(this.employee)
      .subscribe(data=>
        {
          console.log("data added successfully");
          this.employee = new Employee();
          this._route.navigate(['/employee']);
        },
    ) 
    this.tmgCommunicationService.refreshEmitter.emit(true);
  }

  addemployeeformsubmit(){
    this.submitted = true;
    this.save();
    this.gotolist();
  }

  gotolist(){
    this._route.navigate(['/employee']);
  }

  back(){
    this._route.navigate(['/employee']);
  }

}

import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import { Router } from '@angular/router';
import {Supplier} from 'src/app/addsupplier/supplier';
import { NgserviceService } from '../ngservice.service';

@Component({
  selector: 'app-addsupplier',
  templateUrl: './addsupplier.component.html',
  styleUrls: ['./addsupplier.component.css']
})
export class AddsupplierComponent implements OnInit {

  supplier : Supplier = new Supplier();
  submitted = false;
  constructor(private http:HttpClient,private router:Router,private _service: NgserviceService) { }


  
  ngOnInit(): void {
  }

  newSupplier(): void {
    this.submitted = false;
    this.supplier = new Supplier();
  }

  save() {

    console.log(this.supplier)
    this._service.addSupplierToRemote(this.supplier)
      .subscribe(data=>
        {
          console.log("data added successfully");
          this.supplier = new Supplier();
          this.router.navigate(['/supplier']);
        },
  ) 
  }

  addsupplierformsubmit(){
  
    this.submitted = true;
    this.save();
    this.gotolist();
  }

  gotolist(){
    this.router.navigate(['/supplier']);
  }
}

export class Supplier {
    public supplierId: number;
    public supplierName: string;
    public licenseBought: number;
    public urlAddress:string;
}
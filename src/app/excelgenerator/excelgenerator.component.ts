import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx'; 
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-excelgenerator',
  templateUrl: './excelgenerator.component.html',
  styleUrls: ['./excelgenerator.component.css']
})
export class ExcelgeneratorComponent implements OnInit {
  fileName= 'Training.xlsx';
  trainings : any;

  constructor(    private http:HttpClient, private router:Router,
    ) { }

  ngOnInit(): void {
    let resp= this.http.get("http://localhost:8080/completed");
    resp.subscribe((data)=>this.trainings=data);
  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('customers'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
}

import { Component, OnInit } from '@angular/core';
import { EmployeeComponent } from '../employee/employee.component';
import { Employee } from '../employee/Employee';
import { Router, ActivatedRoute } from '@angular/router';
import { NgserviceService } from '../ngservice.service';
import { error } from '@angular/compiler/src/util';
import { Factory } from '../factory/Factory';

@Component({
  selector: 'app-updateemployee',
  templateUrl: './updateemployee.component.html',
  styleUrls: ['./updateemployee.component.css']
})
export class UpdateemployeeComponent implements OnInit {

  id: number;
  employee = new Employee;
  factory:Factory;

  constructor(private _route:Router, private _service:NgserviceService, private _activatedRouter:ActivatedRoute) { }



  ngOnInit(): void {
    this.id=this._activatedRouter.snapshot.params['id'];

    this._service.getEmployee(this.id).subscribe(
      data=>{
        console.log("data received");
        this.employee = data;
      },
      error =>console.log("exception occured")
    )


    this._service.getFactory().subscribe(
      data=>{
        this.factory=data as Factory;
      }
    );
  }

  editEmployee(){
    this._service.updateEmployee(this.id, this.employee).subscribe
    (
      data=>
      {
        console.log(data);
        // this.employee = new Employee();
        this._route.navigate(['/employee']);
      },
      error=>console.log("error")
    )
  }

  onSubmit(){
    this.editEmployee();
  }

  
  cancel(){
    this._route.navigate(['/employee']);
  }
}

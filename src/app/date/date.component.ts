import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmployeeserviceService } from '../employeeservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {
  employees : any;

  constructor(private http:HttpClient, private router:Router) { }

  ngOnInit(): void {
    let resp= this.http.get("http://localhost:8080/getdate");
    resp.subscribe((data)=>this.employees=data);
  }

}

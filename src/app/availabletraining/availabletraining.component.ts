import { Component, OnInit } from '@angular/core';
import { Training } from '../training/Training';
import { Supplier } from '../addsupplier/supplier';
import {NgserviceService} from '../ngservice.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-availabletraining',
  templateUrl: './availabletraining.component.html',
  styleUrls: ['./availabletraining.component.css']
})
export class AvailabletrainingComponent implements OnInit {

  trainings : any;


  constructor(
    private http:HttpClient, private service: NgserviceService,private router:Router,
  ) { }

  ngOnInit(): void {
    let resp= this.http.get("http://localhost:8080/gettrainingdetails");
    resp.subscribe((data)=>this.trainings=data);
  }

  goToEditTraining(id:number){
    console.log('id' + id);
    this.router.navigate(['/updatetraining', id]);
  }

}

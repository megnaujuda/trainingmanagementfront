import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabletrainingComponent } from './availabletraining.component';

describe('AvailabletrainingComponent', () => {
  let component: AvailabletrainingComponent;
  let fixture: ComponentFixture<AvailabletrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabletrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabletrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

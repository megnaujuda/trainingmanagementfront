import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TmgCommunicationService {

    @Output() refreshEmitter = new EventEmitter<boolean>();

    constructor() { }
} 
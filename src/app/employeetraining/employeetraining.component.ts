import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgserviceService} from '../ngservice.service';
import {Employee} from 'src/app/employee/Employee';
import { HttpClient } from '@angular/common/http';
import {EmployeeTraining} from 'src/app/employeetraining/EmployeeTraining';
import { Training } from '../training/Training';


@Component({
  selector: 'app-employeetraining',
  templateUrl: './employeetraining.component.html',
  styleUrls: ['./employeetraining.component.css']
})
export class EmployeetrainingComponent implements OnInit {

  employeetraining : EmployeeTraining = new EmployeeTraining();
  employee:Employee;
  training:Training;

  submitted = false;
  
  constructor(private http:HttpClient,private _route:Router,private _service: NgserviceService) { }

  
  ngOnInit(): void {

    this._service.getemp().subscribe(
      data=>{
        this.employee=data as Employee;
      }
    );

    this._service.gettraining().subscribe(
      data=>{
        this.training=data as Training;
      }
    );
}

newEmployeeTraining(): void {
  this.submitted = false;
  this.employeetraining = new EmployeeTraining();
}

save() {
  console.log(this.employeetraining)
  this._service.addEmployeeTraining(this.employeetraining)
    .subscribe(data=>
      {
        console.log("data added successfully");
        this.employeetraining = new EmployeeTraining();
        this._route.navigate(['/employee']);
      },
) 
}

addemployeeformsubmit(){
  this.submitted = true;
  this.save();
  this.gotolist();
}

gotolist(){
  this._route.navigate(['/employee']);
}


cancel(){
  this._route.navigate(['/employee']);
}
}

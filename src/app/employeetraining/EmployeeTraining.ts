export class EmployeeTraining {
    public employeeTrainingId: number;
    public eligibility: string;
    public trainingIdentified: string;
    public employeeId: number;
    public trainingId: number;
}
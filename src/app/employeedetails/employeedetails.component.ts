import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee/Employee';
import { ActivatedRoute, Router } from '@angular/router';
import { NgserviceService } from '../ngservice.service';
import { EmployeeTraining } from '../employeetraining/EmployeeTraining';

@Component({
  selector: 'app-employeedetails',
  templateUrl: './employeedetails.component.html',
  styleUrls: ['./employeedetails.component.css']
})
export class EmployeedetailsComponent implements OnInit {

  id:number;
  employees:Employee;

  constructor(private route:ActivatedRoute,
    private router:Router,
    private employeeService: NgserviceService) { }

  ngOnInit(): void {

    //     let resp= this.http.get("http://localhost:8080/getemployeedetails"+this.id);
    // resp.subscribe((data)=>this.employees=data);
    
    this.employees=new Employee();
    
    this.id=this.route.snapshot.params['id'];

    this.employeeService.getEmployeeById(this.id)
    .subscribe(data=>{
      console.log(this.id);
      console.log(data)
      this.employees=data as Employee;
      console.log(this.employees);
    },error=>console.log(error));
  }

  list(){
    this.router.navigate(['/employee']);
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, sName: string): any {

    var upperLetter = sName.toUpperCase();
    if(sName===""){
      return value;
    }
    else{
      console.log(value);
      console.log(sName);
      const employeesArray: any[]=[];
    for(let i=0; i<value.length;i++){
      let teamName:string=value[i].visa;
      if(teamName.startsWith(upperLetter)){
        employeesArray.push(value[i])
      }
    }
    return employeesArray;
  }

}
}

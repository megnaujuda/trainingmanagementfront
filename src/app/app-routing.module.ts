import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SupplierComponent } from './supplier/supplier.component';
import { EmployeeComponent } from './employee/employee.component';
import { from } from 'rxjs';
import { AddsupplierComponent } from './addsupplier/addsupplier.component';
import { AddemployeeComponent } from './addemployee/addemployee.component';
import { UpdateemployeeComponent } from './updateemployee/updateemployee.component';
import { FactoryComponent } from './factory/factory.component';
import { EmployeedetailsComponent } from './employeedetails/employeedetails.component';
import { TrainingComponent } from './training/training.component';
import {  EmployeetrainingComponent } from './employeetraining/EmployeeTraining.component';
import { UpdatetrainingComponent } from './updatetraining/updatetraining.component';
import { AvailabletrainingComponent } from './availabletraining/availabletraining.component';
import { DateComponent } from './date/date.component';
import { ExcelgeneratorComponent } from './excelgenerator/excelgenerator.component';


const routes: Routes = [
  {path: '', redirectTo: '/date', pathMatch: 'full'}, 
  {path: 'supplier', component: SupplierComponent},
  {path: 'addsupplier', component: AddsupplierComponent},
  {path: 'addemployee', component: AddemployeeComponent},
  {path: 'updateemployee/:id', component: UpdateemployeeComponent},
  {path: 'details/:id', component: EmployeedetailsComponent},
  {path: 'employee', component: EmployeeComponent},
  {path: 'factory', component: FactoryComponent},
  {path: 'training', component: TrainingComponent},
  {path: 'employeetraining', component: EmployeetrainingComponent},
  {path: 'availabletraining', component: AvailabletrainingComponent},
  {path: 'updatetraining/:id', component: UpdatetrainingComponent},
  {path: 'date', component: DateComponent},
  {path: 'excelgenerator', component: ExcelgeneratorComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
export const routingComponents=[EmployeedetailsComponent,SupplierComponent,EmployeeComponent,AddsupplierComponent,UpdateemployeeComponent,AddemployeeComponent,FactoryComponent,TrainingComponent,EmployeetrainingComponent,UpdatetrainingComponent,AvailabletrainingComponent,DateComponent,ExcelgeneratorComponent];

import { Component, OnInit } from '@angular/core';
import {Training} from 'src/app/training/Training';
import { Router } from '@angular/router';
import {NgserviceService} from '../ngservice.service';
import {Employee} from 'src/app/employee/Employee';
import { HttpClient } from '@angular/common/http';
import { Supplier } from '../addsupplier/supplier';
import { TrainingCategory } from 'src/app/training/TrainingCatgory';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {

 
  training : Training = new Training();
  supplier:Supplier;
  trainingcategory:TrainingCategory;

  submitted = false;

  constructor(private http:HttpClient,private _route:Router,private _service: NgserviceService) { }

  ngOnInit(): void {

    this._service.getSupplier().subscribe(
      data=>{
        this.supplier=data as Supplier;
      }
    );

    
    this._service.gettrainingcategory().subscribe(
      data=>{
        this.trainingcategory=data as TrainingCategory;
      }
    );
}



newTraining(): void {
  this.submitted = false;
  this.training = new Training();
}

save() {
  console.log(this.training)
  this._service.addTraining(this.training)
    .subscribe(data=>
      {
        console.log("data added successfully");
        this.training = new Training();
        this._route.navigate(['/employee']);
      },
) 
}

addtrainingformsubmit(){
  this.submitted = true;
  this.save();
  this.gotolist();
}

gotolist(){
  this._route.navigate(['/availabletraining']);
}
cancel(){ 
  this._route.navigate(['/availabletraining']);
}


}

export class Training {
    public trainingId: number;
    public trainingName: string;
    public proposedTraining: string;    
    public trainingStatus: string;
    public startDate: string;
    public endDate: string;
    public licenseAttributed: string;
    public duration: string;
    public mandatory: string;
    public supplierId: number;
    public trainingcatId: number;
}
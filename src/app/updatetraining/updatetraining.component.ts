import { Component, OnInit } from '@angular/core';
import { Training } from '../training/Training';
import { EmployeeTraining } from '../employeetraining/EmployeeTraining';
import { Employee } from '../employee/Employee';
import { Supplier } from '../addsupplier/supplier';
import { TrainingCategory } from '../training/TrainingCatgory';
import { Router, ActivatedRoute } from '@angular/router';
import { NgserviceService } from '../ngservice.service';

@Component({
  selector: 'app-updatetraining',
  templateUrl: './updatetraining.component.html',
  styleUrls: ['./updatetraining.component.css']
})
export class UpdatetrainingComponent implements OnInit {
  id: number;
  training = new Training;
  supplier:Supplier;
  trainingcategory:TrainingCategory;


  constructor(private _route:Router, private _service:NgserviceService, private _activatedRouter:ActivatedRoute) { }

  ngOnInit(): void {
    this.id=this._activatedRouter.snapshot.params['id'];

    this._service.getTrainingById(this.id).subscribe(
      data=>{
        console.log("data received");
        this.training = data;
      },
      error =>console.log("exception occured")
    )


    this._service.getSupplier().subscribe(
      data=>{
        this.supplier=data as Supplier;
      }
    );


    this._service.gettrainingcategory().subscribe(
      data=>{
        this.trainingcategory=data as TrainingCategory;
      }
    );
  }

  editTraining(){
    this._service.updateTraining(this.id, this.training).subscribe
    (
      data=>
      {
        console.log(data);
        // this.employee = new Employee();
        this._route.navigate(['/availabletraining']);
      },
      error=>console.log("error")
    )
  }

  onSubmit(){
    this.editTraining();
  }


  cancel(){
    this._route.navigate(['/availabletraining']);
  }
}

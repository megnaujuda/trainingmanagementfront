import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgserviceService } from '../ngservice.service';


@Component({
  selector: 'app-factory',
  templateUrl: './factory.component.html',
  styleUrls: ['./factory.component.css']
})
export class FactoryComponent implements OnInit {

  factories : any;
  factoryacademy:any;
  factoryipension:any;
  factorynet:any;
  factoryadmin:any;

  
  constructor(private http:HttpClient, private _factoryService: NgserviceService,private router:Router) { }

  ngOnInit(): void {
    let resp= this.http.get("http://localhost:8080/getfactorydetails");
    resp.subscribe((data)=>this.factories=data);

    let academy= this.http.get("http://localhost:8080/getacademydetails");
    academy.subscribe((data)=>this.factoryacademy=data);

    let ipension= this.http.get("http://localhost:8080/getipensiondetails");
    ipension.subscribe((data)=>this.factoryipension=data);

    let net= this.http.get("http://localhost:8080/getnetdetails");
    net.subscribe((data)=>this.factorynet=data);

    let admin= this.http.get("http://localhost:8080/getadmindetails");
    admin.subscribe((data)=>this.factoryadmin=data);

  }

}

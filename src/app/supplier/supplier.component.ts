import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {


  suppliers : any;

  constructor(private http:HttpClient,private route:Router) { }

  
  ngOnInit(): void {

    let resp= this.http.get("http://localhost:8080/api/suppliers");
    resp.subscribe((data)=>this.suppliers=data);
  }

  goToAddSupplier(){
    this.route.navigate(['/addsupplier']);
  }

}

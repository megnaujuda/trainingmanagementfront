import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Supplier } from './addsupplier/supplier';
import { Employee } from './employee/Employee';
import { Training } from './training/Training';

import {environment} from 'src/environments/environment';
import { EmployeeTraining } from './employeetraining/EmployeeTraining';


@Injectable({
  providedIn: 'root'
})
export class NgserviceService {
  private baseUrl='';
  constructor(private _http:HttpClient) { }

  fetchEmployeeListFromRemote():Observable<any>{
    return this._http.get(`${environment.baseUrl}/getemployee`, {headers:{'Content-Type': 'application/json'}});
  }

  getemp():Observable<any>{
    return this._http.get(`${environment.baseUrl}/api/employees`, {headers:{'Content-Type': 'application/json'}});
  }

  gettraining():Observable<any>{
    return this._http.get(`${environment.baseUrl}/gettraininglist`, {headers:{'Content-Type': 'application/json'}});
  }

  gettrainingcategory():Observable<any>{
    return this._http.get(`${environment.baseUrl}/api/trainingcategory`, {headers:{'Content-Type': 'application/json'}});
  }

  deleteProject(id: number): Observable<any> {
    return this._http.delete<any>(`${environment.baseUrl}/api/deleteemployee` + id );
  }

  addSupplierToRemote(supplier : Supplier):Observable<any>{
    return this._http.post<any>(`${environment.baseUrl}/api/addsupplier`, supplier, {headers:{'Content-Type': 'application/json'}});
  }

  addEmployeeToRemote(employee : Employee):Observable<any>{
    return this._http.post<any>(`${environment.baseUrl}/api/addemployee`, employee, {headers:{'Content-Type': 'application/json'}});
  }

  getEmployee(id:number):Observable<any>{
    return this._http.get<any>(`${environment.baseUrl}/api/getemployeebyid/` + id, {headers:{'Content-Type': 'application/json'}});
  }

  getEmployeeById(id:number):Observable<any>{
    return this._http.get<any>(`${environment.baseUrl}/getemployeebyid/` + id, {headers:{'Content-Type': 'application/json'}});
  }

  updateEmployee(id: number, employee : Employee):Observable<any>{
    return this._http.put<any>(`${environment.baseUrl}/api/updateemployee/` + id, employee, {headers:{'Content-Type': 'application/json'}});
  }

  getFactory():Observable<any>{
    return this._http.get<any>(`${environment.baseUrl}/getfactorylist`);
  }

  addEmployeeTraining(employeetraining : EmployeeTraining):Observable<any>{
    return this._http.post<any>(`${environment.baseUrl}/api/addemployeetraining`, employeetraining, {headers:{'Content-Type': 'application/json'}});
  }

  getSupplier():Observable<any>{
    return this._http.get<any>(`${environment.baseUrl}/api/suppliers`);
  }

  addTraining(training : Training):Observable<any>{
    return this._http.post<any>(`${environment.baseUrl}/addtraining`, training, {headers:{'Content-Type': 'application/json'}});
  }

  getTrainingById(id:number):Observable<any>{
    return this._http.get<any>(`${environment.baseUrl}/gettrainingbyid/` + id, {headers:{'Content-Type': 'application/json'}});
  }

  updateTraining(id: number, training : Training):Observable<any>{
    return this._http.put<any>(`${environment.baseUrl}/updatetraining/` + id, training, {headers:{'Content-Type': 'application/json'}});
  }

  getDate():Observable<any>{
    return this._http.get<any>(`${environment.baseUrl}/getdate`);
  }
}

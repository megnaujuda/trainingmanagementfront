export class Employee {
    public employeeId: number;
    public firstName: string;
    public lastName: string;
    public employeeStatus: string;
    public visa: string;
    public email: string;
    public factory: number;
}
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { fromEventPattern, Subscription } from 'rxjs';
import { EmployeeserviceService } from '../employeeservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import {TmgCommunicationService} from 'src/app/communication/tmg-communication.emitter';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit, OnDestroy {

  employees : any;
  searchName:string="";
  employeeSubscription: Subscription;

  
  constructor(private http:HttpClient, private _employeeService: EmployeeserviceService,private router:Router,
    private TmgCommunicationService: TmgCommunicationService) { }

  ngOnInit(): void {
    let resp= this.http.get("http://localhost:8080/getemployeedetails");
    resp.subscribe((data)=>this.employees=data);
    this.employeeSubscription = this.TmgCommunicationService.refreshEmitter.subscribe((data => {
      console.log(data);
    }))
  }

  ngOnDestroy(): void {
    this.employeeSubscription.unsubscribe();
  }



  deleteEmployee(id: number) {
    this._employeeService.deleteEmployee(id).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.debug('Deleted successfully');
      },
      error => console.log('Execption occured')
    // tslint:disable-next-line: semicolon
    )
  }

  goToAddEmployee(id:number){
    this.router.navigate(['/addemployee']);
  }

  goToEditEmployee(id:number){
    console.log('id' + id);
    this.router.navigate(['/updateemployee', id]);
  }

  employeeDetails(id:number){
    this.router.navigate(['details',id]);
  }

  goToEditTraining(id:number){
    this.router.navigate(['/updatetraining',id]);
  }

}
